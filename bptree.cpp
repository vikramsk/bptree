#include<iostream>
#include<vector>
#include<fstream>
#include<string>
#include<sstream>
#include<stack>

using namespace std;

// Node defines the base class for the 
// different types of Nodes.
class Node {
    public: 
        enum EnumNodeType{NODE_INTERNAL = 1, NODE_LEAF = 2};
        
        EnumNodeType node_type;

        Node(int capacity, EnumNodeType nodeType) : capacity(capacity), node_type(nodeType) {}       
        int capacity;
};

// NodeLeaf defines the lowest level nodes
// of the B+ Tree.
class NodeLeaf: public Node {
    public: 
        // KeyValue defines the primary storage
        // container for a given Key.
        // A given key can have multiple values.
        struct KeyValue {
            double key;
            vector<string>* values;
        };

        NodeLeaf(int capacity, Node::EnumNodeType nodeType, vector<KeyValue> keyValuePairs) : 
            Node(capacity, nodeType), data(keyValuePairs), prevNode(NULL), nextNode(NULL){}

        vector<KeyValue> data;
        NodeLeaf* prevNode;
        NodeLeaf* nextNode;
};

// NodeInternal defines the higher level 
// nodes of the B+ Tree. This node doesn't 
// store the data for a given Key.
class NodeInternal: public Node {
    public:
        NodeInternal(int capacity, Node::EnumNodeType nodeType, vector<double> keys, vector<Node*> node_pointers) : 
            Node(capacity, nodeType), keys(keys), node_pointers(node_pointers) {}

        vector<double> keys;
        vector<Node*> node_pointers;
};

// BPTree defines the structure for the 
// B+ tree. 
// The B+ Tree supports 2 primary operations:
// Search(By Value, By Range), Insert.
class BPTree {
    public:
        
        // Commands supported on the B+ Tree.
        static const string CMD_SEARCH;
        static const string CMD_INSERT;

        // BPTree is the parameterized constructor for
        // initializing the B+ Tree. It accepts the
        // order of the tree as the input.
        BPTree(int order) : order(order), rootNode(NULL) {}
       
        // Insert adds the given key, value pair
        // to the B+ Tree.
        void Insert(double key, string value) {
            stack<Node*> nodeStack = node_path(key);
            
            // empty tree.
            if (nodeStack.empty()) {
                NodeLeaf::KeyValue data = {
                    key,
                    new vector<string>{value}
                };
                rootNode = new NodeLeaf(order, Node::NODE_LEAF, {data});
                return;
            }

            // first node on the top of the stack is guaranteed
            // to be a leaf node.
            NodeLeaf* leaf = (NodeLeaf*)nodeStack.top();
            nodeStack.pop();

            // newLeaf is passed in case a new leaf node is created
            // after the key insertion.
            newNodeInfo newNode;

            newNode = insert_data(leaf, key, value);
            if (newNode.node_pointer == NULL) {
                return;
            }
            
            while(!nodeStack.empty()) {
                NodeInternal* n = (NodeInternal*)nodeStack.top();
                nodeStack.pop();
                newNode = insert_index(n, newNode.node_pointer, newNode.index_key);
                if (newNode.node_pointer == NULL) {
                    return;
                }
            }

            if (newNode.node_pointer != NULL) {
                rootNode = new NodeInternal(order, Node::NODE_INTERNAL, {newNode.index_key}, {rootNode, newNode.node_pointer});
            }
            
            return;
        }
        
        string cleanDouble(double value) {
            string str = std::to_string (value);
            str.erase ( str.find_last_not_of('0') + 1, std::string::npos );
            str.erase ( str.find_last_not_of('.') + 1, std::string::npos );
            return str;
        }

        // Search returns the values associated with 
        // the given key.
        string Search(double key)  {
            vector<NodeLeaf::KeyValue> keyValues = range_search(key, key);
            string output; 
            if (keyValues.size() == 0) {
                return "Null"; 
            }

            for(int i=0; i<keyValues[0].values->size(); i++) {
                output += keyValues[0].values->at(i) + ",";
            }
            output = output.substr(0, output.size()-1);

            return output;
        }
        
        // Search returns the key value pairs for keys 
        // which lie within the given key range.
        string Search(double keyStart, double keyEnd) {
            vector<NodeLeaf::KeyValue> keyValues = range_search(keyStart, keyEnd);
            string output;

            if (keyValues.size() == 0) {
                return "Null";
            }

            for(int i=0; i<keyValues.size(); i++) {
                for(int j=0; j<keyValues[i].values->size(); j++) {
                    output += "(" + cleanDouble(keyValues[i].key) + "," + keyValues[i].values->at(j) + "),";
                }
            }
            output = output.substr(0, output.size()-1);

            return output;
        }

    private:
     
        // order defines the order of the n-ary
        // B+ Tree.
        int order;
       
        // rootNode stores the root node of the 
        // B+ Tree.
        Node* rootNode;

        // newNodeInfo defines the structure for
        // storing the information for a new node.
        struct newNodeInfo{

            // index_key is the key that needs to
            // be inserted at the higher level.
            double index_key;

            // node_pointer is a pointer to the new
            // node created at the lower level.
            Node* node_pointer;
        };

        // range_search is used to search the B+ Tree for the objects within
        // the given start and end keys.
        // It returns a vector of KeyValues for all keys within the range.
        vector<NodeLeaf::KeyValue> range_search(double keyStart, double keyEnd) {
            stack<Node*> nodeStack = node_path(keyStart);
            vector<NodeLeaf::KeyValue> keyValues;
            
            if (nodeStack.empty()) {
                return keyValues;
            }

            NodeLeaf* leaf = (NodeLeaf*)nodeStack.top();
            
            bool continueSearch = true;
            while(true) {
                for(int i = 0; i < leaf->data.size(); i++) {
                    if (leaf->data[i].key < keyStart) {
                        continue;
                    }

                    if (leaf->data[i].key > keyEnd) {
                        continueSearch = false;
                        break;
                    }

                    keyValues.push_back(leaf->data[i]);
                }

                if (!continueSearch) {
                    break;
                }
                 
                leaf = leaf->nextNode;
                if (leaf == NULL) {
                    break;
                }
            }

            return keyValues;
        }

        // insert_index is used to insert a key, node pointer to the child node
        // into an internal node in the B+ Tree.
        newNodeInfo insert_index(NodeInternal* node, Node* childNode, double key) {
            newNodeInfo newNode = newNodeInfo{0.0, NULL};
            vector<double>::iterator it;
            int i=0;
            for (it = node->keys.begin(); it != node->keys.end() && *it < key; it++) {
                i++;
            }
            
            if (it == node->keys.end()) {
                node->keys.push_back(key);
                node->node_pointers.push_back(childNode);
            } else {
                it = node->keys.insert(node->keys.begin()+i, key);
                node->node_pointers.insert(node->node_pointers.begin()+i+1, childNode);
            }
           
            // the node needs to be split if it reaches it's capacity.
            if (node->keys.size() == node->capacity) {
                return split_int_node(node);
            }

            return newNode;
        }
        
        // split_int_node is used to split an internal node.
        // it returns a newNodeInfo. this contains the information
        // of the newly created internal node.
        newNodeInfo split_int_node(NodeInternal* node) {
            double splitValue = node->keys[node->capacity/2];
            vector<double>::iterator startKey = node->keys.begin() + node->capacity/2 + 1;
            vector<double>::iterator endKey = node->keys.end();
            vector<double> newKeys(startKey, endKey);

            vector<Node*>::iterator startNodePointer = node->node_pointers.begin() + node->capacity/2 + 1;
            vector<Node*>::iterator endNodePointer = node->node_pointers.end();
            vector<Node*> newNodePointers(startNodePointer, endNodePointer);

            NodeInternal* childNode = new NodeInternal(node->capacity, Node::NODE_INTERNAL, newKeys ,newNodePointers);

            node->keys.erase(node->keys.begin()+node->capacity/2, node->keys.end());
            node->node_pointers.erase(node->node_pointers.begin() + node->capacity/2 + 1, node->node_pointers.end());

            return newNodeInfo{splitValue, childNode};
        }

        // insert_data inserts the key, value pair in the given leaf node.
        // it returns a newNodeInfo. this contains the information of the
        // newly created leaf node.
        newNodeInfo insert_data(NodeLeaf* node, double key, string value) {
            newNodeInfo newNode = newNodeInfo{0.0, NULL};
            vector<NodeLeaf::KeyValue>::iterator it;
            it = node->data.begin();
            for (it = node->data.begin(); it != node->data.end() && it->key < key; it++) {
            }

            NodeLeaf::KeyValue kv = {
                key,
                new vector<string>{value}
            };

            if (it == node->data.end()) {
                node->data.push_back(kv);
            } else {
                if (it->key == key) {
                    it->values->push_back(value);
                    return newNode;
                }
                it = node->data.insert(it, kv);
            }
            
            if (node->data.size() == node->capacity) {
               return split_leaf(node);
            }
            //print_leaf(node);
            return newNode;
        }

        // split_leaf splits a leaf node into two nodes when it 
        // hits its defined capacity.
        newNodeInfo split_leaf(NodeLeaf* currLeaf) {
            vector<NodeLeaf::KeyValue>::iterator start = currLeaf->data.begin() + currLeaf->capacity/2;
            vector<NodeLeaf::KeyValue>::iterator end = currLeaf->data.end();
           
            vector<NodeLeaf::KeyValue> newData(start, end);
            NodeLeaf* newLeaf = new NodeLeaf(currLeaf->capacity, Node::NODE_LEAF, newData);

            newLeaf->nextNode = currLeaf->nextNode;
            newLeaf->prevNode = currLeaf;
            currLeaf->nextNode = newLeaf;
            
            // clear the key value pairs that have been moved to the new leaf node.
            currLeaf->data.erase(currLeaf->data.begin()+currLeaf->capacity/2, currLeaf->data.end());
            
            return newNodeInfo{newLeaf->data.begin()->key, newLeaf};
        }

        // node_path returns the stack of nodes which
        // are traversed from the root to the leaf
        // node. The last visited node is on the top
        // of the stack.
        stack<Node*> node_path(double key) {
            stack<Node*> nodeStack;
            Node* node = rootNode;
            
            if (node == NULL) {
                return nodeStack;
            }
            
            while(node->node_type != Node::NODE_LEAF) {
                nodeStack.push(node);
                int i = 0; 
                NodeInternal* n = (NodeInternal*)node;
                
                for(i=0; i<n->keys.size() && n->keys[i] < key; i++) {}
                
                if (i != n->keys.size() && key >= n->keys[i]) 
                    i++;
                        
                node = n->node_pointers[i];
            }

            // adding the leaf node to the stack.
            nodeStack.push(node);
            return nodeStack;
        }
};


const string BPTree::CMD_SEARCH = "Search";
const string BPTree::CMD_INSERT = "Insert";

// split accepts a string and a delimiter to return
// the string tokens separated by the delimiter.
vector<string> split(const string &s, char delim) {
    stringstream ss(s);
    string item;
    vector<string> tokens;
    while (getline(ss, item, delim)) {
        tokens.push_back(item);
    }
    return tokens;
}

// to_double converts a string to double.
double to_double(const string &s) {
    stringstream ss(s);
    double data = 0.0;

    ss>>data;
    return data;
}

int main(int argc, char *argv[]) {
    if (argc > 3) {
        cout<<"usage: "<<argv[0]<<"<input filename>\n";
        return 0;
    }
    
    const char* output_file = "output_file.txt";
    if (argc == 3) {
        output_file = argv[2];
    }
    
    // Redirecting the std input, output streams
    // to the input and output files.
    freopen(argv[1], "r", stdin);
    freopen(output_file, "w", stdout);

    int order;
    cin>>order;
    
    BPTree bpTree(order);
    string command;
    while(cin>>command) {
        vector<string> stringTokens;
        stringTokens = split(command, '(');
        
        string cmdName = stringTokens[0];
        stringTokens[1] = stringTokens[1].substr(0, stringTokens[1].size()-1);

        vector<string> funcArgs;
        funcArgs = split(stringTokens[1], ',');
        
        if (cmdName == BPTree::CMD_INSERT) {
            bpTree.Insert(to_double(funcArgs[0]), funcArgs[1]);
            continue;     
        } 
       
        string output;
        if (funcArgs.size() == 2) {
            output = bpTree.Search(to_double(funcArgs[0]), to_double(funcArgs[1]));
        } else {
            output = bpTree.Search(to_double(funcArgs[0]));
        }
        cout<<output<<endl;
    }
    return 0;
}
